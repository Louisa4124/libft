/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 10:19:05 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/17 18:02:45 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char	*ft_strndup(const char *s1, size_t len)
{
	size_t		tmp;
	char		*cpy;

	tmp = 0;
	if (!len)
		return (NULL);
	while (s1[tmp] && tmp < len)
		tmp++;
	cpy = (char *)malloc((tmp + 1) * sizeof(char));
	if (!cpy)
		return (NULL);
	tmp = 0;
	while (s1[tmp] && tmp < len)
	{
		cpy[tmp] = s1[tmp];
		tmp++;
	}
	cpy[tmp] = '\0';
	return (cpy);
}

char	*ft_strtrim(char const *s1, char const *set)
{
	char	*trim;
	size_t	end;

	if (!s1 || !set)
		return (NULL);
	if (!ft_strlen(s1))
	{
		trim = malloc(sizeof(char) * 1);
		trim[0] = '\0';
		return (trim);
	}
	while (*s1 && ft_strchr(set, *s1))
		s1++;
	end = ft_strlen(s1);
	while (s1[end - 1] && ft_strchr(set, s1[end - 1]))
		end--;
	trim = ft_strndup(s1, end);
	if (!trim)
		return (NULL);
	return (trim);
}
