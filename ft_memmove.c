/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 11:39:11 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/15 11:27:04 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t			size;
	unsigned char	*tmp;
	unsigned char	*tmp2;

	tmp = (unsigned char *)dst;
	tmp2 = (unsigned char *)src;
	size = 0;
	if (tmp > tmp2)
	{
		while (size < len)
		{
			len--;
			tmp[len] = tmp2[len];
		}
	}
	else
	{
		while (size < len)
		{
			tmp[size] = tmp2[size];
			size++;
		}
	}
	return (dst);
}
