/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 12:50:45 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/17 15:20:29 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *s1)
{
	int		tmp;
	char	*cpy;

	tmp = 0;
	while (s1[tmp])
		tmp++;
	cpy = (char *)malloc((tmp + 1) * sizeof(char));
	if (!cpy)
		return (NULL);
	tmp = 0;
	while (s1[tmp])
	{
		cpy[tmp] = s1[tmp];
		tmp++;
	}
	cpy[tmp] = '\0';
	return (cpy);
}
