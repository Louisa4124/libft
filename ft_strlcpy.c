/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 12:58:16 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/11 12:02:54 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dest, const char *src, size_t dstsize)
{
	size_t	tmp;
	size_t	n;

	tmp = 0;
	n = 0;
	if (dstsize != 0)
	{
		while (src[tmp] != '\0')
		{
			if (tmp < (dstsize - 1))
			{
				dest[tmp] = src[tmp];
				n++;
			}
			tmp++;
		}
	}
	else
	{
		while (src[tmp] != '\0')
			tmp++;
		return (tmp);
	}
	dest[n] = '\0';
	return (tmp);
}
