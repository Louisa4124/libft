/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 10:40:52 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/15 12:45:41 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_sizen(int n)
{
	int	size;

	if (!n)
		return (1);
	size = 0;
	while (n > 0)
	{
		size++;
		n /= 10;
	}
	return (size);
}

void	ft_fill(char *str, int n, int size, int i)
{
	while (size >= i)
	{
		str[size] = (n % 10) + 48;
		n = n / 10;
		size--;
	}
	return ;
}

char	*ft_itoa(int n)
{
	int		size;
	int		i;
	char	*str;

	i = 0;
	if (n == -2147483648)
		return ("-2147483648");
	if (n < 0)
	{
		i++;
		n = -n;
	}
	size = ft_sizen(n);
	str = (char *)malloc((size + 1 + i) * sizeof(char));
	if (!str)
		return (NULL);
	str[size + i] = 0;
	if (i == 1)
		str[0] = '-';
	size += i - 1;
	ft_fill(str, n, size, i);
	return (str);
}
