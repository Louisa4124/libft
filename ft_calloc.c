/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_calloc.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 17:05:02 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/17 13:26:22 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_calloc(size_t count, size_t size)
{
	unsigned char	*dst;
	size_t			i;

	i = 0;
	dst = malloc(sizeof(size) * count);
	if (!dst)
		return (NULL);
	while (dst[i] && i < count)
	{
		dst[i] = 0;
		i++;
	}
	return ((void *)dst);
}
