/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/09 12:54:14 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/15 14:05:58 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t	tmp;

	tmp = 0;
	while (s1[tmp] != '\0' && s2[tmp] != '\0' && tmp < n)
	{
		if (s1[tmp] < s2[tmp])
			return (-1);
		if (s1[tmp] > s2[tmp])
			return (1);
		tmp++;
	}
	if (s1[tmp] == '\0' && s2[tmp] != '\0' && n != 0)
		return (-1);
	if (s2[tmp] == '\0' && s1[tmp] != '\0' && n != 0)
		return (1);
	return (0);
}
