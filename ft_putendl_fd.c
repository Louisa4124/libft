/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 11:07:02 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/15 13:28:59 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putendl_fd(char *s, int fd)
{
	int	tmp;

	tmp = 0;
	while (s[tmp])
	{
		write(fd, &s[tmp], 1);
		tmp++;
	}
	write(fd, "\n", 1);
}
