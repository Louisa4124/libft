/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/11 09:27:55 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/17 17:45:08 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dest, const char *src, size_t dstsize)
{
	size_t	size_dest;
	size_t	size_src;
	size_t	i;

	size_dest = ft_strlen(dest);
	size_src = ft_strlen(src);
	i = 0;
	if (dstsize == 0)
		return (size_src);
	while (src[i] && size_dest + i < dstsize - 1)
	{
		dest[size_dest + i] = src[i];
		i++;
	}
	if (size_dest > dstsize)
		return (size_src + dstsize);
	dest[size_dest + i] = '\0';
	return (size_dest + size_src);
}
