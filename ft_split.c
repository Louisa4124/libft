/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 10:39:19 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/17 17:05:36 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "stdio.h"

static char	*ft_strndup(char *src, int n)
{
	int		tmp;
	char	*cpy;

	cpy = (char *)malloc((n + 1) * sizeof(char));
	if (!cpy)
		return (NULL);
	tmp = 0;
	while (tmp < n)
	{
		cpy[tmp] = src[tmp];
		tmp++;
	}
	cpy[tmp] = '\0';
	return (cpy);
}	

static void	ft_freesplit(size_t i, char **split)
{
	while (i >= 0)
	{
		free(split[i]);
		split[i] = NULL;
		i--;
	}
	free(split);
	split = NULL;
}

static int	ft_get_size(char *str, char c)
{
	int	i;

	i = 0;
	while (str[i] && (str[i] != c))
		i++;
	return (i);
}

static int	ft_wordcount(char *str, char c)
{
	int	w;

	w = 0;
	while (*str)
	{
		while (*str == c)
			str++;
		if (*str)
			w++;
		while (*str != c && *str)
			str++;
	}
	return (w);
}

char	**ft_split(char *str, char c)
{
	char	**t;
	int		size;
	int		i;

	size = ft_wordcount(str, c);
	t = (char **)malloc((size + 1) * sizeof(char *));
	if (t == NULL)
		return (NULL);
	i = 0;
	while (*str && i < size)
	{
		while (*str == c)
			str++;
		t[i] = ft_strndup(str, ft_get_size(str, c));
		if (!t[i])
		{
			ft_freesplit(i, t);
			return (NULL);
		}
		str += ft_get_size(str, c);
		i++;
	}
	t[size] = 0;
	return (t);
}
