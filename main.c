/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/08 13:35:42 by tlegrand          #+#    #+#             */
/*   Updated: 2022/11/15 12:56:02 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


// #include <stdio.h>
// #include <ctype.h>
// #include <string.h>
// #include <stdlib.h>
// #include <limits.h>
// #include "libft.h"

// //Fonctions pt1
// int	check_isalpha(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (isalpha(c) != ft_isalpha(c))
// 		{
// 			printf("isalpha ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("isalpha = %d\n", isalpha(c));
// 			printf("ft_isalpha = %d\n", ft_isalpha(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_isdigit(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (isdigit(c) != ft_isdigit(c))
// 		{
// 			printf("isdigit ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("isdigit = %d\n", isdigit(c));
// 			printf("ft_isdigit = %d\n", ft_isdigit(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_isalnum(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (isalnum(c) != ft_isalnum(c))
// 		{
// 			printf("isalnum ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("isalnum = %d\n", isalnum(c));
// 			printf("ft_isalnum = %d\n", ft_isalnum(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_isascii(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (isascii(c) != ft_isascii(c))
// 		{
// 			printf("isascii ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("isascii = %d\n", isascii(c));
// 			printf("ft_isascii = %d\n", ft_isascii(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_isprint(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (isprint(c) != ft_isprint(c))
// 		{
// 			printf("isprint ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("isprint = %d\n", isprint(c));
// 			printf("ft_isprint = %d\n", ft_isprint(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_strlen(void)
// {
// 	char	*str;

// 	str = "";
// 	if (strlen(str) != ft_strlen(str))
// 	{
// 		printf("strlen ERROR\n");
// 		printf("str : %s\n", str);
// 		printf("strlen = %lu\n", strlen(str));
// 		printf("ft_strlen = %lu\n", ft_strlen(str));
// 		return (1);
// 	}
// 	str = "salut ";
// 	if (strlen(str) != ft_strlen(str))
// 	{
// 		printf("strlen ERROR\n");
// 		printf("str : %s\n", str);
// 		printf("strlen = %lu\n", strlen(str));
// 		printf("ft_strlen = %lu\n", ft_strlen(str));
// 		return (1);
// 	}
// 	return (0);
// }

// int	check_memset(void)
// {
// 	char	memstr[40] = "Bonjour chere amie, comment allez-vous?";
// 	char	ftmemstr[40] = "Bonjour chere amie, comment allez-vous?";
// 	char	memstr2[0];
// 	char	ftmemstr2[0];
// 	void	*a;
// 	void	*b;

// 	a = memset(memstr + 5, '+', 10 * sizeof(char));
// 	b = ft_memset(ftmemstr + 5, '+', 10 * sizeof(char));
// 	if (a != memstr + 5 || b != ftmemstr + 5)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("memset =%s\n", a);
// 		printf("ft_memset =%s\n", b);
// 		return (1);
// 	}
// 	if (memcmp(memstr, ftmemstr, strlen(memstr)))
// 	{
// 		printf("set ERROR\n");
// 		printf("memstr : %s\n", memstr);
// 		printf("ftmemstr = %s\n", ftmemstr);
// 		return (1);
// 	}

// 	a = memset(memstr2, '+', 0 * sizeof(char));
// 	b = ft_memset(ftmemstr2, '+', 0 * sizeof(char));
// 	if (a != memstr2 || b != ftmemstr2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("memset =%s\n", a);
// 		printf("ft_memset =%s\n", b);
// 		return (1);
// 	}
// 	if (memcmp(memstr, ftmemstr, strlen(memstr)))
// 	{
// 		printf("set ERROR\n");
// 		printf("memstr : %s\n", memstr);
// 		printf("ftmemstr = %s\n", ftmemstr);
// 		return (1);
// 	}




// 	return (0);
// }

// int	check_bzero(void)
// {
// 	char	bzstr[40] = "Bonjour chere amie, comment allez-vous?";
// 	char	ftbzstr[40] = "Bonjour chere amie, comment allez-vous?";

// 	bzero(bzstr + 5, (0));
// 	ft_bzero(ftbzstr + 5, 0);
// 	if (memcmp(bzstr, ftbzstr, strlen(bzstr)))
// 	{
// 		printf("set ERROR\n");
// 		printf("bzstr : %s\n", bzstr);
// 		printf("ftbzstr = %s\n", ftbzstr);
// 		return (1);
// 	}
// 	bzero(bzstr + 5, 10);
// 	ft_bzero(ftbzstr + 5, 10);
// 	if (memcmp(bzstr, ftbzstr, strlen(bzstr)))
// 	{
// 		printf("set ERROR\n");
// 		printf("bzstr : %s\n", bzstr);
// 		printf("ftbzstr = %s\n", ftbzstr);
// 		return (1);
// 	}
// 	return (0);
// }

// int	check_strlcpy(void)
// {
// 	char	*src = "0123456789abcdef";
// 	char	dst1[20];
// 	char	dst2[20];
// 	int		size;

// //test dstsize < srcsize
// 	size = 10;
// 	if (strlcpy(dst1, src, size) != ft_strlcpy(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcpy = %lu\n", strlcpy(dst1, src, size));
// 		printf("ft_strlcpy = %lu\n", ft_strlcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcpy = %s\n", dst1);
// 		printf("dest ft_strlcpy = %s\n", dst2);
// 	}
// //test dstsize > srcsize
// 	size = 20;
// 	if (strlcpy(dst1, src, size) != ft_strlcpy(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcpy = %lu\n", strlcpy(dst1, src, size));
// 		printf("ft_strlcpy = %lu\n", ft_strlcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcpy = %s\n", dst1);
// 		printf("dest ft_strlcpy = %s\n", dst2);
// 	}
// //test size = 0
// 	size = 0;
// 	if (strlcpy(dst1, src, size) != ft_strlcpy(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcpy = %lu\n", strlcpy(dst1, src, size));
// 		printf("ft_strlcpy = %lu\n", ft_strlcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcpy = %s\n", dst1);
// 		printf("dest ft_strlcpy = %s\n", dst2);
// 	}
// //test src overlap 
// /*	size = 10;
// 	if (strlcpy(src, src + 3, size) != ft_strlcpy(src, src + 3, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcpy = %lu\n", strlcpy(dst1, src, size));
// 		printf("ft_strlcpy = %lu\n", ft_strlcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcpy = %s\n", dst1);
// 		printf("dest ft_strlcpy = %s\n", dst2);
// 	}*/
// //test src null
// 	src = "";
// 	size = 10;
// 	if (strlcpy(dst1, src, size) != ft_strlcpy(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcpy = %lu\n", strlcpy(dst1, src, size));
// 		printf("ft_strlcpy = %lu\n", ft_strlcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcpy = %s\n", dst1);
// 		printf("dest ft_strlcpy = %s\n", dst2);
// 	}
// 	return (0);
// }

// int	check_strlcat(void)
// {
// 	char	*src = "0123456789abcdef";
// 	char	dst1[20] = "bonjour mon chou";
// 	char	dst2[20] = "bonjour mon chou";
// 	int		size;

// //test dstsize < srcsize
// 	size = 10;
// 	bzero(dst1, 20);
// 	bzero(dst2, 20);
// 	if (strlcat(dst1, src, size) != ft_strlcat(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcat = %lu\n", ft_strlcat(dst1, src, size));
// 		printf("ft_strlcat = %lu\n", ft_strlcat(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CAT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcat = %s\n", dst1);
// 		printf("dest ft_strlcat = %s\n", dst2);
// 		return (1);
// 	}

// //test dstsize > srcsize
// 	size = 20;
// 	bzero(dst1, 20);
// 	bzero(dst2, 20);
// 	if (strlcat(dst1, src, size) != ft_strlcat(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcat = %lu\n", ft_strlcat(dst1, src, size));
// 		printf("ft_strlcat = %lu\n", ft_strlcat(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CAT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcat = %s\n", dst1);
// 		printf("dest ft_strlcat = %s\n", dst2);
// 		return (1);
// 	}

// //test dstsize = 0
// 	size = 0;
// 	bzero(dst1, 20);
// 	bzero(dst2, 20);
// 	if (strlcat(dst1, src, size) != ft_strlcat(dst2, src, size))
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("strlcat = %lu\n", ft_strlcat(dst1, src, size));
// 		printf("ft_strlcat = %lu\n", ft_strlcat(dst2, src, size));
// 		return (1);
// 	}
// 	if (strncmp(dst1, dst2, strlen(dst1)))
// 	{
// 		printf("CAT ERROR\n");
// 		printf("src : %s\n", src);
// 		printf("dest strlcat = %s\n", dst1);
// 		printf("dest ft_strlcat = %s\n", dst2);
// 		return (1);
// 	}
// 	return (0);
// }

// int	check_toupper(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (toupper(c) != ft_toupper(c))
// 		{
// 			printf("toupper ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("toupper = %d\n", toupper(c));
// 			printf("ft_toupper = %d\n", ft_toupper(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_tolower(void)
// {
// 	unsigned char	c;

// 	c = -1;
// 	while (c < 255)
// 	{
// 		if (tolower(c) != ft_tolower(c))
// 		{
// 			printf("tolower ERROR\n");
// 			printf("c : %c\n", c);
// 			printf("tolower = %d\n", tolower(c));
// 			printf("ft_tolower = %d\n", ft_tolower(c));
// 			return (1);
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_strchr(void)
// {
// 	char	str[12] = "abbcdef451d";
// 	int	c;

// 	c = 0;
// 	while ( c <= 256)
// 	{
// 		if (strchr(str, c))
// 		{
// 			if (ft_strchr(str, c))
// 			{
// 				if (strcmp(strchr(str, c), ft_strchr(str, c)))
// 				{
// 					printf("strchr ERROR\n");
// 					printf("c = %d\n", c);
// 					printf("strchr =  %s\n", strchr(str, c));
// 					printf("ft_strchr =  %s\n", ft_strchr(str, c));
// 					return (1);
// 				}
// 			}
// 			else 
// 			{
// 				printf("strchr ERROR\n");
// 				printf("c = %d\n", c);
// 				printf("strchr =  %s\n", strchr(str, c));
// 				printf("ft_strchr =  NULL\n");
// 				return (1);
// 			}
// 		}
// 		else
// 		{
// 			if (ft_strchr(str, c))
// 			{
// 				printf("strchr ERROR\n");
// 				printf("c = %d\n", c);
// 				printf("strchr =  NULL\n");
// 				printf("ft_strchr =  %s\n", ft_strchr(str, c));
// 				return (1);
// 			}
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_strrchr(void)
// {
// 	char	str[12] = "abbcdef451d";
// 	int		c;

// 	c = 0;
// 	while ( c <= 256)
// 	{
// 		if (strrchr(str, c))
// 		{
// 			if (ft_strrchr(str, c))
// 			{
// 				if (strcmp(strrchr(str, c), ft_strrchr(str, c)))
// 				{
// 					printf("strrchr ERROR\n");
// 					printf("c = %d\n", c);
// 					printf("strrchr =  %s\n", strrchr(str, c));
// 					printf("ft_strrchr =  %s\n", ft_strrchr(str, c));
// 					return (1);
// 				}
// 			}
// 			else 
// 			{
// 				printf("strrchr ERROR\n");
// 				printf("c = %d\n", c);
// 				printf("strrchr =  %s\n", strrchr(str, c));
// 				printf("ft_strrchr =  NULL\n");
// 				return (1);
// 			}
// 		}
// 		else
// 		{
// 			if (ft_strrchr(str, c))
// 			{
// 				printf("strrchr ERROR\n");
// 				printf("c = %d\n", c);
// 				printf("strrchr =  NULL\n");
// 				printf("ft_strrchr =  %s\n", ft_strrchr(str, c));
// 				return (1);
// 			}
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_strncmp(void)
// {
// 	char	*s1;
// 	char	*s2;
// 	size_t	i;

// 	s1 = "abcdef";
// 	s2 = s1;
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (strncmp(s1, s2, i) != ft_strncmp(s1, s2, i))
// 		{
// 			printf("strncmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("strncmp = %d\n", strncmp(s1, s2, i));
// 			printf("ft_strncmp = %d\n", ft_strncmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcdef";
// 	s2 = "hus";
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (strncmp(s1, s2, i) != ft_strncmp(s1, s2, i))
// 		{
// 			printf("strncmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("strncmp = %d\n", strncmp(s1, s2, i));
// 			printf("ft_strncmp = %d\n", ft_strncmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcdefs";
// 	s2 = "abcdefg122224";
// 	i = 0;
// 	while (i < 20)
// 	{
// 		if (strncmp(s1, s2, i) != ft_strncmp(s1, s2, i))
// 		{
// 			printf("strncmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("strncmp = %d\n", strncmp(s1, s2, i));
// 			printf("ft_strncmp = %d\n", ft_strncmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "";
// 	s2 = "";
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (strncmp(s1, s2, i) != ft_strncmp(s1, s2, i))
// 		{
// 			printf("strncmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("strncmp = %d\n", strncmp(s1, s2, i));
// 			printf("ft_strncmp = %d\n", ft_strncmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "test\200";
// 	s2 = "test\0";
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (strncmp(s1, s2, i) != ft_strncmp(s1, s2, i))
// 		{
// 			printf("strncmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("strncmp = %d\n", strncmp(s1, s2, i));
// 			printf("ft_strncmp = %d\n", ft_strncmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}
// 	return (0);
// }

// int	check_strnstr(void)
// {
// 	char	*s1;
// 	char	*s2;
// 	size_t	i;

// 	s1 = "abcdef";
// 	s2 = s1;
// 	i = -1;
// 	while (i < 10)
// 	{
// 		if (strnstr(s1, s2, i) != ft_strnstr(s1, s2, i))
// 		{
// 			printf("strnstr ERROR\n");
// 			printf("haystack :%s\tneedle :%s\tn: %lu\n", s1, s2, i);
// 			printf("strnstr = %s\n", strnstr(s1, s2, i));
// 			printf("ft_strnstr = %s\n", ft_strnstr(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcdef";
// 	s2 = "cd";
// 	i = -1;
// 	while (i < 10)
// 	{
// 		if (strnstr(s1, s2, i) != ft_strnstr(s1, s2, i))
// 		{
// 			printf("strnstr ERROR\n");
// 			printf("haystack :%s\tneedle :%s\tn: %lu\n", s1, s2, i);
// 			printf("strnstr = %s\n", strnstr(s1, s2, i));
// 			printf("ft_strnstr = %s\n", ft_strnstr(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcbcbcd";
// 	s2 = "bcbcd";
// 	i = -1;
// 	while (i < 10)
// 	{
// 		if (strnstr(s1, s2, i) != ft_strnstr(s1, s2, i))
// 		{
// 			printf("strnstr ERROR\n");
// 			printf("haystack :%s\tneedle :%s\tn: %lu\n", s1, s2, i);
// 			printf("strnstr = %s\n", strnstr(s1, s2, i));
// 			printf("ft_strnstr = %s\n", ft_strnstr(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcdef";
// 	s2 = "";
// 	i = -1;
// 	while (i < 10)
// 	{
// 		if (strnstr(s1, s2, i) != ft_strnstr(s1, s2, i))
// 		{
// 			printf("strnstr ERROR\n");
// 			printf("haystack :%s\tneedle :%s\tn: %lu\n", s1, s2, i);
// 			printf("strnstr = %s\n", strnstr(s1, s2, i));
// 			printf("ft_strnstr = %s\n", ft_strnstr(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "";
// 	s2 = "jdieddff";
// 	i = -1;
// 	while (i < 10)
// 	{
// 		if (strnstr(s1, s2, i) != ft_strnstr(s1, s2, i))
// 		{
// 			printf("strnstr ERROR\n");
// 			printf("haystack :%s\tneedle :%s\tn: %lu\n", s1, s2, i);
// 			printf("strnstr = %s\n", strnstr(s1, s2, i));
// 			printf("ft_strnstr = %s\n", ft_strnstr(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "";
// 	s2 = "";
// 	i = -1;
// 	while (i < 10)
// 	{
// 		if (strnstr(s1, s2, i) != ft_strnstr(s1, s2, i))
// 		{
// 			printf("strnstr ERROR\n");
// 			printf("haystack :%s\tneedle :%s\tn: %lu\n", s1, s2, i);
// 			printf("strnstr = %s\n", strnstr(s1, s2, i));
// 			printf("ft_strnstr = %s\n", ft_strnstr(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}
// 	return (0);
// }

// int	check_atoi(void)
// {
// 	char	*s1;

// 	s1 = "def";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "de+154";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "   +569del 41";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "\n -156";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "0";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "  -0";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = " \t -597ef4";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi = %d\n", atoi(s1));
// 		printf("ft_atoi = %d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "-+257ef4";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi =%d\n", atoi(s1));
// 		printf("ft_atoi =%d\n", ft_atoi(s1));
// 		return (1);
// 	}

// 	s1 = "+-257ef4";
// 	if (atoi(s1) != ft_atoi(s1))
// 	{
// 		printf("atoi ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("atoi =%d\n", atoi(s1));
// 		printf("ft_atoi =%d\n", ft_atoi(s1));
// 		return (1);
// 	}
// 	return (0);
// }

// int	check_memcmp(void)
// {
// 	char	*s1;
// 	char	*s2;
// 	size_t	i;

// 	s1 = "abcdef";
// 	s2 = s1;
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (memcmp(s1, s2, i) != ft_memcmp(s1, s2, i))
// 		{
// 			printf("memcmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("memcmp = %d\n", memcmp(s1, s2, i));
// 			printf("ft_memcmp = %d\n", ft_memcmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcdef";
// 	s2 = "abc45";
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (memcmp(s1, s2, i) != ft_memcmp(s1, s2, i))
// 		{
// 			printf("memcmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("memcmp = %d\n", memcmp(s1, s2, i));
// 			printf("ft_memcmp = %d\n", ft_memcmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "";
// 	s2 = s1;
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (memcmp(s1, s2, i) != ft_memcmp(s1, s2, i))
// 		{
// 			printf("memcmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("memcmp = %d\n", memcmp(s1, s2, i));
// 			printf("ft_memcmp = %d\n", ft_memcmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}

// 	s1 = "abcd 4f";
// 	s2 = "abcdefgh";
// 	i = 0;
// 	while (i < 10)
// 	{
// 		if (memcmp(s1, s2, i) != ft_memcmp(s1, s2, i))
// 		{
// 			printf("memcmp ERROR\n");
// 			printf("s1 :%s\ts2 :%s\tn: %lu\n", s1, s2, i);
// 			printf("memcmp = %d\n", memcmp(s1, s2, i));
// 			printf("ft_memcmp = %d\n", ft_memcmp(s1, s2, i));
// 			return (1);
// 		}
// 		i++;
// 	}
// 	return (0);
// }

// int	check_memchr(void)
// {
// 	char	*str;
// 	int		c;
// 	size_t	i;

// 	str = "abbcdefd451d";
// 	c = 0;
// 	while (c < 127)
// 	{
// 		i = 0;
// 		while (i < 15)
// 		{
// 			if (memchr(str, c, i))
// 			{
// 				if (ft_memchr(str, c, i))
// 				{
// 					if (memcmp(memchr(str, c, i), ft_memchr(str, c, i), i))
// 					{
// 						printf("memchr ERROR\n");
// 						printf("c = %d\ti = %lu\n", c, i);
// 						printf("memchr =  %s\n", memchr(str, c, i));
// 						printf("ft_memchr =  %s\n", ft_memchr(str, c, i));
// 						return (1);
// 					}
// 				}
// 				else
// 				{
// 					printf("memchr ERROR\n");
// 					printf("c = %d\ti = %lu\n", c, i);
// 					printf("memchr =  %s\n", memchr(str, c, i));
// 					printf("ft_memchr =  NULL\n");
// 					return (1);
// 				}
// 			}
// 			else
// 			{
// 				if (ft_memchr(str, c, i))
// 				{
// 					printf("memchr ERROR\n");
// 					printf("c = %d\ti = %lu\n", c, i);
// 					printf("memchr =  NULL\n");
// 					printf("ft_memchr =  %s\n", ft_memchr(str, c, i));
// 					return (1);
// 				}
// 			}
// 			i++;
// 		}
// 		c++;
// 	}
// 	return (0);
// }

// int	check_memcpy(void)
// {
// 	char	*src = "0123456789abcdef";
// 	char	dst1[20];
// 	char	dst2[20];
// 	size_t	size;

// //	bzero(dst1, 20);
// //	bzero(dst2, 20);
// //test dstsize < srcsize
// 	size = 10;
// 	memcpy(dst1, src, size);
// 	ft_memcpy(dst2, src, size);
// 	if (ft_memcpy(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memcpy = %p\n", ft_memcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memcpy = %s\n", dst1);
// 		printf("dest ft_memcpy = %s\n", dst2);
// 		return (1);
// 	}
// //test dstsize > srcsize
// 	size = 20;
// 	memcpy(dst1, src, size);
// 	ft_memcpy(dst2, src, size);
// 	if (ft_memcpy(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memcpy = %s\n", ft_memcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memcpy = %s\n", dst1);
// 		printf("dest ft_memcpy = %s\n", dst2);
// 		return (1);
// 	}
// //test size = 0
// 	size = 0;
// 	memcpy(dst1, src, size);
// 	ft_memcpy(dst2, src, size);
// 	if (ft_memcpy(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memcpy = %s\n", ft_memcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memcpy = %s\n", dst1);
// 		printf("dest ft_memcpy = %s\n", dst2);
// 		return (1);
// 	}

// //test src null
// 	src = "";
// 	size = 10;
// 	memcpy(dst1, src, size);
// 	ft_memcpy(dst2, src, size);
// 	if (ft_memcpy(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memcpy = %s\n", ft_memcpy(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memcpy = %s\n", dst1);
// 		printf("dest ft_memcpy = %s\n", dst2);
// 		return (1);
// 	}
// 	return (0);
// }

// int	check_memmove(void)
// {
// 	char	*src = "0123456789abcdef";
// 	char	dst1[20];
// 	char	dst2[20];
// 	size_t	size;

// //	bzero(dst1, 20);
// //	bzero(dst2, 20);
// //test dstsize < srcsize
// 	size = 10;
// 	memmove(dst1, src, size);
// 	ft_memmove(dst2, src, size);
// 	if (ft_memmove(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memmove = %p\n", ft_memmove(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memmove = %s\n", dst1);
// 		printf("dest ft_memmove = %s\n", dst2);
// 		return (1);
// 	}
// //test dstsize > srcsize
// 	size = 20;
// 	memmove(dst1, src, size);
// 	ft_memmove(dst2, src, size);
// 	if (ft_memmove(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memmove = %s\n", ft_memmove(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memmove = %s\n", dst1);
// 		printf("dest ft_memmove = %s\n", dst2);
// 		return (1);
// 	}
// //test size = 0
// 	size = 0;
// 	memmove(dst1, src, size);
// 	ft_memmove(dst2, src, size);
// 	if (ft_memmove(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memmove = %s\n", ft_memmove(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memmove = %s\n", dst1);
// 		printf("dest ft_memmove = %s\n", dst2);
// 		return (1);
// 	}

// //test src null
// 	src = "";
// 	size = 10;
// 	memmove(dst1, src, size);
// 	ft_memmove(dst2, src, size);
// 	if (ft_memmove(dst2, src, size) != dst2)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memmove = %s\n", ft_memmove(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memmove = %s\n", dst1);
// 		printf("dest ft_memmove = %s\n", dst2);
// 		return (1);
// 	}
// /*//test src overlap 
// 	size = 10;
// 	memmove(dst1, src, size);
// 	ft_memmove(dst2, src, size);
// 	if (ft_memmove(src, src + 3, size) != src)
// 	{
// 		printf("OUTPUT ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dst = %p\n", dst2);
// 		printf("out ft_memmove = %s\n", ft_memmove(dst2, src, size));
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, size))
// 	{
// 		printf("CPY ERROR\n");
// 		printf("src : %s\tsize : %lu\n", src, size);
// 		printf("dest memmove = %s\n", dst1);
// 		printf("dest ft_memmove = %s\n", dst2);
// 		return (1);
// 	}*/
// 	return (0);
// }

// int	check_strdup(void)
// {
// 	char	*src;
// 	char	*dst1;
// 	char	*dst2;

// 	src = "Hey hey hey!  ..";
// 	dst1 = strdup(src);
// 	dst2 = ft_strdup(src);
// 	if (!dst1 || !dst2)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (memcmp(dst1, dst2, strlen(src)))
// 	{
// 		printf("dup ERROR\n");
// 		printf("src :%s\n", src);
// 		printf("strdup :%s\n", dst1);
// 		printf("ft_strdup :%s\n", dst2);
// 		return (1);
// 	}
// 	return (0);
// }

// int	check_calloc(void)
// {
// 	size_t	n;
// 	void	*ptr1;
// 	void	*ptr2;

// 	n = 11;
// 	ptr1 = calloc(n, sizeof(int));
// 	ptr2 = ft_calloc(n, sizeof(int));
// 	if (!ptr1 || !ptr2)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (memcmp(ptr1, ptr2, n))
// 	{
// 		printf("fill ERROR\n");
// 		printf("calloc :%s\n", ptr1);
// 		printf("ft_calloc :%s\n", ptr2);
// 		return (1);
// 	}
// 	return (0);
// }

// //fonctions pt2
// int	check_substr(void)
// {
// 	char			*s1;
// 	char			*s2;
// 	char			*wanted;
// 	unsigned int	start;
// 	size_t			len;

// 	s1 = "tell me how to dieand sufferrriieieieie!";
// 	start = 8;
// 	len = 10;
// 	wanted = "how to die";
// 	s2 = ft_substr(s1, start, len);
// 	if (!s2)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (strncmp(s2, wanted, len))
// 	{
// 		printf("substr ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("wanted :%s\n", wanted);
// 		printf("substr(str, %d, %zu) : %s\n\n", start, len, s2);
// 		return (1);
// 	}

// 	s1 = "tell me how to dieand sufferrriieieieie!";
// 	start = 22;
// 	len = 1;
// 	wanted = "s";
// 	s2 = ft_substr(s1, start, len);
// 	if (!s2)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (strncmp(s2, wanted, len))
// 	{
// 		printf("substr ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("wanted :%s\n", wanted);
// 		printf("substr(str, %d, %zu) : %s\n\n", start, len, s2);
// 		return (1);
// 	}

// 	s1 = "tell me how to dieand sufferrriieieieie!";
// 	start = 50;
// 	len = 10;
// 	wanted = "";
// 	s2 = ft_substr(s1, start, len);
// 	if (!s2)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (strncmp(s2, wanted, len))
// 	{
// 		printf("substr ERROR\n");
// 		printf("str :%s\n", s1);
// 		printf("wanted :%s\n", wanted);
// 		printf("substr(str, %d, %zu) : %s\n\n", start, len, s2);
// 		return (1);
// 	}

// 	return (0);
// }

// int	check_strjoin(void)
// {
// 	char	*pref;
// 	char	*suff;
// 	char	*fufufusion;

// 	pref = "bonjour com";
// 	suff = "ment vas-tu?";
// 	fufufusion = ft_strjoin(pref, suff);
// 	if (!fufufusion)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	printf("preffixe :%s\tsuffixe :%s\n", pref, suff);
// 	printf("strjoin :%s\n", fufufusion);
// 	return (0);
// }

// int	check_strtrim(void)
// {
// 	char	*set;
// 	char	*s;
// 	char	*strimed;
// 	char	*want;

// 	set = ". -+/*";
// 	s = " + hello + my old .. friend*! **";
// 	want = "hello + my old .. friend*!";
// 	strimed = ft_strtrim(s, set);
// 	if (!strimed)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (strncmp(strimed, want, strlen(want)))
// 	{
// 		printf("str :%s\n", s);
// 		printf("set a trim:%s\n", set);
// 		printf("result :%s\n\n", strimed);
// 		return (1);
// 	}

// 	set = " *";
// 	s = " **  *** *";
// 	want = "";
// 	strimed = ft_strtrim(s, set);
// 	if (!strimed)
// 	{
// 		printf("malloc ERROR\n");
// 		return (1);
// 	}
// 	if (strncmp(strimed, want, strlen(want)))
// 	{
// 		printf("str :%s\n", s);
// 		printf("set a trim:%s\n", set);
// 		printf("result :%s\n\n", strimed);
// 		return (1);
// 	}
// 	want = "coucou";


// 	return (0);
// }

// int	check_split(void)
// {
// 	char	*str;
// 	char	sep;
// 	char	**split;
// 	int		i;

// 	str = "...c'est...un.ciel.etoile.!...";
// 	sep = '.';
// 	split = ft_split(str, sep);
// 	if (!split)
// 	{
// 		printf("malloc ou str error\n");
// 		return (1);
// 	}
// 	i = 0;
// 	while (split[i])
// 	{
// 		printf("split[%d]: %s\n", i, split[i]);
// 		free(split[i]);
// 		i++;
// 	}

// 	str = "...........";
// 	sep = '.';
// 	split = ft_split(str, sep);
// 	if (!split)
// 	{
// 		printf("split malloc ou str erro2\n\n");
// 		return (1);
// 	}
// 	i = 0;
// 	while (split[i])
// 	{
// 		printf("split[%d]: %s\n", i, split[i]);
// 		free(split[i]);
// 		i++;
// 	}

// 	str = "";
// 	sep = 's';
// 	split = ft_split(str, sep);

// 	if (!split)
// 	{
// 		printf("split malloc ou str error3\n\n");
// 		return (1);
// 	}
// 	i = 0;
// 	while (split[i])
// 	{
// 		printf("split[%d]: %s\n", i, split[i]);
// 		free(split[i]);
// 		i++;
// 	}
// 	free(split);
// 	return (0);
// }

// int	check_itoa(void)
// {
// 	int		n;
// 	char	*str;

// 	n = 78;
// 	str = ft_itoa(n);
// 	if (!str)
// 	{
// 		printf("malloc error\n");
// 		return (1);
// 	}
// 	printf("n: %d,\titoa: %s\n", n, str);

// 	n = -n;
// 	str = ft_itoa(n);
// 	if (!str)
// 	{
// 		printf("malloc error\n");
// 		return (1);
// 	}
// 	printf("n: %d,\titoa: %s\n", n, str);

// 	n = 0;
// 	str = ft_itoa(n);
// 	if (!str)
// 	{
// 		printf("malloc error\n");
// 		return (1);
// 	}
// 	printf("n: %d,\titoa: %s\n", n, str);

// 	n = INT_MAX;
// 	str = ft_itoa(n);
// 	if (!str)
// 	{
// 		printf("malloc error\n");
// 		return (1);
// 	}
// 	printf("n: %d,\titoa: %s\n", n, str);

// 	n = INT_MIN;
// 	str = ft_itoa(n);
// 	if (!str)
// 	{
// 		printf("malloc error\n");
// 		return (1);
// 	}
// 	printf("n: %d,\titoa: %s\n", n, str);

// 	n = -2147483648LL;
// 	str = ft_itoa(n);
// 	if (!str)
// 	{
// 		printf("malloc error\n");
// 		return (1);
// 	}
// 	printf("n: %d,\titoa: %s\n\n", n, str);

// 	return (0);
// }

// void	check_putnbr_fd(void)
// {
// 	printf("check_ft_putnbr_fd, fd = 1\n");
// 	printf("n = 457\n");
// 	ft_putnbr_fd(457, 1);
// 	printf("\nn = 0\n");
// 	ft_putnbr_fd(0, 1);
// 	printf("\nn = -1578\n");
// 	ft_putnbr_fd(-1578, 1);
// 	printf("\nn = 4\n");
// 	ft_putnbr_fd(4, 1);
// 	printf("\nn = -4846677\n");
// 	ft_putnbr_fd(-4846677, 1);
// 	printf("\nn = 13575864\n");
// 	ft_putnbr_fd(13575864, 1);
// 	printf("\n");

// }

// void	check_ncmp(void)
// {
// 	char	s1[7] = "bonjour";
// 	char	s2[7] = "bonj51";
// 	int		i = 10;
	
// 	if (strncmp(s1, s2, i) != strncmp("bonjour", "bonj51", 10))
// 	{
// 		printf("strncmp with s1, s2, i : %d\n", strncmp(s1, s2, i));
// 		printf("strncmp direct string: %d\n", strncmp("bonjour", "bonj51", 10));
// 	}
// }

// int	main(void)
// {
// 	if (!check_isalpha())
// 		printf("ft_isalpha OK\n\n");
// 	if (!check_isdigit())
// 		printf("ft_isdigit OK\n\n");
// 	if (!check_isalnum())
// 		printf("ft_isalnum OK\n\n");
// 	if (!check_isascii())
// 		printf("ft_isascii OK\n\n");
// 	if (!check_isprint())
// 		printf("ft_isprint OK\n\n");
// 	if (!check_strlen())
// 		printf("ft_strlen OK\n\n");
// 	if (!check_memset())
// 		printf("ft_memset OK\n\n");
// 	if (!check_bzero())
// 		printf("ft_bzero OK\n\n");
// 	if (!check_strlcpy())
// 		printf("ft_strlcpy OK\n\n");
// 	if (!check_strlcat())
// 		printf("ft_strlcat OK\n\n");
// 	if (!check_toupper())
// 		printf("ft_toupper OK\n\n");
// 	if (!check_tolower())
// 		printf("ft_tolower OK\n\n");
// 	if (!check_strchr())
// 		printf("ft_strchr OK\n\n");
// 	if (!check_strrchr())
// 		printf("ft_strrchr OK\n\n");
// 	if (!check_strncmp())
// 		printf("ft_strncmp OK\n\n");
// 	if (!check_strnstr())
// 		printf("ft_strnstr OK\n\n");
// 	if (!check_atoi())
// 		printf("ft_atoi OK\n\n");
// 	if (!check_memcmp())
// 		printf("ft_memcmp OK\n\n");
// 	if (!check_memchr())
// 		printf("ft_memchr OK\n\n");
// 	if (!check_memcpy())
// 		printf("ft_memcpy OK\n\n");
// 	if (!check_memmove())
// 		printf("ft_memmove OK\n\n");
// 	if (!check_strdup())
// 		printf("ft_strdup OK\n\n");
// 	if (!check_calloc())
// 		printf("ft_calloc OK\n\n");
// 	if (!check_substr())
// 		printf("ft_substr OK\n\n");
// 	if (!check_strjoin())
// 		printf("ft_strjoin OK?\n\n");
// 	if (!check_strtrim())
// 		printf("ft_strtrim OK\n\n");
// 	if (!check_split())
// 		printf("ft_split OK?\n\n");
// //	if (!check_itoa())
// //		printf("ft_itoa OK?\n\n");
// //	check_putnbr_fd();
// /*
// 	if (!ft_strrchr("bonjour", 's'))
// 		printf("NULL\n");
// 	printf("ft_itoa(%d) : %s\n", INT_MIN, ft_itoa(INT_MIN));

// 	void	*t;
// 	void	*p;
// 	size_t	count;
// 	size_t	size;

// 	count = SIZE_MAX;
// 	size = 10;
// 	t = calloc(count, size);
// 	if (t)
// 		printf("calloc pas NULL\n");
// 	else
// 		printf("calloc NULL\n");
// 	p = ft_calloc(count, size);
// 	if (p)
// 		printf("ft_calloc pas NULL\n");
// 	else
// 		printf("ft_calloc NULL\n");
// 	if (t && p && memcmp(t, p, count))
// 	{
// 		printf("ERROR\n");
// 		printf("count : %lu\tsize : %lu\tmulti : %lu\n", count, size, count * size);
// 		printf("cmp : %d", memcmp(t, p, count));
// 	}
// 	else
// 		printf("ils sont pareil\n");


// */
// 	return (0);
// }
