/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                               +#+#+#+#+#+   +#+           */
/*   Created: 2022/11/10 09:44:23 by lboudjem          #+#    #+#             */
/*   Updated: 2022/11/17 13:51:52 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlen(const char *s)
{
	size_t	size;

	size = 0;
	while (s[size])
		size++;
	return (size);
}

char	*ft_strjoin(char const *s1, char const *s2)
{
	char		*res;
	size_t		tmp;
	size_t		tmp2;

	res = (char *)malloc((ft_strlen(s1) + ft_strlen(s2) + 1) * sizeof(char));
	if (!res)
		return (NULL);
	tmp = 0;
	tmp2 = 0;
	while (tmp < ft_strlen(s1))
	{
		res[tmp] = s1[tmp];
		tmp++;
	}
	while (tmp < ft_strlen(s1) + ft_strlen(s2))
	{
		res[tmp] = s2[tmp2];
		tmp2++;
		tmp++;
	}
	res[ft_strlen(s1) + ft_strlen(s2)] = '\0';
	return (res);
}
